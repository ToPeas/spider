const axios = require("axios")
const cheerio = require("cheerio")

const crawl = async url => {
  return axios({
    method: "get",
    url
  }).then(res => {
    if (res.status === 200) {
      const body = res.data
      const $ = cheerio.load(body)
      $("#posts .post-title a").each((index, ele) => {
        const title = $(ele)
          .text()
          .trim()
        console.log(title)
      })
    }
  })
}

crawl("http://aevit.xyz")
