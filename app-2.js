const http = require("http")
const fsExtra = require("fs-extra")
const fs = require("fs")
const path = require("path")
const axios = require("axios")
const cheerio = require("cheerio")
const iconv = require("iconv-lite")

const arr = []

const rootDownDir = path.join(__dirname, '/download')


// 获取图片的地址
const fetchImgSrc = url => {
  return axios({
    method: "get",
    url
  }).then(res => {
    const body = res.data
    const $ = cheerio.load(body)
    const page = $('#page a').eq(-2).text()
    $(".main #content a img").each((index, ele) => {
      downImg($(ele).attr('src'))
    })
  })
}

// 下载图片地址
const downImg = url => {
  const pathArr = url.split("/")
  const dirName = pathArr[pathArr.length - 2]
  const imgName = pathArr[pathArr.length - 1]
  const path = rootDownDir + '/' + dirName + '/' + imgName
  return fsExtra.pathExists(path).then(exist => {
    if (!exist) {
      return fsExtra.ensureFile(path).then(() => {
        return axios({
          method: "get",
          url,
          responseType: "stream",
          headers: {
            Referer: url //  防反跨站
          }
        })
      }).then(res => {
        return res.data.pipe(fs.createWriteStream(path))
      }).then(() => {
        console.log('下载文件成功')
      }).catch(err => {
        console.log(err)
      })
    } else {
      console.log("文件已经存在")
    }
  })
}

fetchImgSrc("http://www.mmjpg.com/mm/1192")